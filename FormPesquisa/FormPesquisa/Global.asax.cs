﻿using FormPesquisa.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace FormPesquisa
{
    public class MvcApplication : System.Web.HttpApplication
    {
        /*protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }*/

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        /*protected void Application_AcquireRequestState(object sender, EventArgs ea)
        {
            var path = HttpContext.Current.Request.Path;
            if (!path.Contains("/Home/Login"))
            {
                var userId = Helpers.UserLogin.GetUserId();
                if (userId == 0)
                    HttpContext.Current.Response.Redirect("/Home/Login");
            }
        }*/
    }
}
