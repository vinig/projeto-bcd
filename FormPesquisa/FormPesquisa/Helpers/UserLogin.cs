﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using Core.Database.System;

namespace FormPesquisa.Helpers
{
    public class UserLogin
    {

        internal static void SetUser(Usuario User)
        {
            FormsAuthentication.SetAuthCookie(User.id.ToString(), false);
        }

        internal static Usuario GetUser()
        {
            var users = new Usuario().All();
            var currentUser = new Usuario().GetByCondition("where id = @id", new { id = GetUserId() });
            if (currentUser.Any())
                return currentUser.First();

            LogoutUser();
            return null;
        }

        internal static void LogoutUser()
        {
            FormsAuthentication.SignOut();
            HttpContext.Current.Session.Clear();
        }

        internal static int GetUserId()
        {
            var sid = HttpContext.Current.User.Identity.Name;
            int id = 1;
            int.TryParse(sid, out id);
            return id;
        }
    }
}