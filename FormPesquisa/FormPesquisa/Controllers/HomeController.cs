﻿using Core.Database.System;
using System.Collections.Generic;
using System.Web.Mvc;
using System;
using System.Linq;
using Core.Database;

namespace FormPesquisa.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewData["Times"] = new Times().TimesAll();
            ViewData["Campeao"] = new Campeoes().CampeoesAll();
            ViewData["Jogos"] = new Jogos().JogosAll();
            ViewData["TimesJogos"] = new Times().All();
            ViewData["Jogadores"] = new Jogadores().JogadoresAll();
            ViewData["Posicoes"] = new Posicoes().All();


            ViewData["PaisCount"] = new Paises().Count();
            ViewData["LigaCount"] = new Ligas().Count();
            ViewData["PatrocinadorCount"] = new Patrocinadores().Count();
            return View();
        }
        public ActionResult Login()
        {
            return View();
        }

        public ActionResult Time(int? id = 0)
        {
            ViewData["Paises"] = new Paises().All();
            ViewData["Ligas"] = new Ligas().All();

            if (id != 0)
                ViewData["Time"] = new Times().Get((int)id);

            return View();
        }

        [HttpPost]
        public ActionResult Time(Times obj, int? deletar=0)
        {
            if(deletar == 0)
                new Times().TimesIU(obj);
            else
                new Times().TimeDeletar(obj);

            return RedirectToAction("Index");
        }
        public ActionResult Campeao(int? id = 0)
        {
            ViewData["Times"] = new Times().All();

            if (id != 0)
                ViewData["Campeao"] = new Campeoes().Get((int)id);

            return View();
        }

        [HttpPost]
        public ActionResult Campeao(Campeoes obj, int? deletar = 0)
        {
            if (deletar == 0)
                new Campeoes().CampeoesIU(obj);
            else
                new Campeoes().CampeaoDeletar(obj);

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult InsertNovos(string nome, string nomeL, string nomeP, string imagem)
        {
            if (nome != null && nome != "")
                new Paises().Insert(nome);
            if (nomeL != null && nomeL != "")
                new Ligas().Insert(nomeL);
            if (nomeP != null && nomeP != "")
                new Patrocinadores().Insert(nomeP, imagem);

            return RedirectToAction("Index");
        }

        public ActionResult Partida(int? id = 0)
        {
            ViewData["Times"] = new Times().All();
            ViewData["Ligas"] = new Ligas().All();
            ViewData["Paises"] = new Paises().All();

            if (id != 0)
                ViewData["Jogos"] = new Jogos().Get((int)id);

            return View();
        }

        [HttpPost]
        public ActionResult Partida(Jogos obj, int? deletar = 0)
        {
            if (deletar == 0)
                new Jogos().JogoIU(obj);
            else
                new Jogos().JogoDeletar(obj);

            return RedirectToAction("Index");
        }

        public ActionResult Jogador(int? id = 0)
        {
            ViewData["Times"] = new Times().All();
            ViewData["Ligas"] = new Ligas().All();
            ViewData["Paises"] = new Paises().All();
            ViewData["Posicao"] = new Posicoes().All();

            if (id != 0)
                ViewData["Jogador"] = new Jogadores().Get((int)id);

            return View();
        }

        [HttpPost]
        public ActionResult Jogador(Jogadores obj, int? deletar = 0)
        {
            if (deletar == 0)
                new Jogadores().JogadorIU(obj);
            else
                new Jogadores().JogadorDeletar(obj);

            return RedirectToAction("Index");
        }

        public ActionResult Codigo()
        {
            return View();
        }

        public ActionResult CodigoEspecifico(int? id = 0)
        {
            ViewData["id"] = id;
            ViewData["Avg"] = new Posicoes().PosicoesAVG();
            ViewData["Sum"] = new Jogadores().JogadoresSUM();
            ViewData["BetWeen"] = new Jogadores().JogadoresBetWeen(1, 12);
            ViewData["And"] = new Paises().PaisesAND();
            ViewData["Or"] = new Ligas().LigasOR();
            ViewData["Not"] = new Jogadores().JogadoresNOT();
            ViewData["Join"] = new Times().TimesJOIN();
            return View();
        }

        public ActionResult Entidade(int? type = 0)
        {
            ViewData["type"] = type;
            ViewData["Posicao"] = new Posicoes().PosicaoAll();
            ViewData["Usuario"] = new Usuario().All();
             
            return View();
        }

        [HttpPost]
        public ActionResult Login(string user, string pass)
        {
            var bdUser = new Usuario().GetByCondition("where nome = @user AND senha = @pass",
                new { user, pass });
            if (bdUser.Any())
            {
                //Helpers.UserLogin.SetUser(bdUser.First());
                return RedirectToAction("Index");
            } else
                TempData["message"] = "Usuário e/ou Senha inválidos!";
            return RedirectToAction("Login");
        }

        [HttpPost]
        public ActionResult NewUser(string user, string pass, string email)
        {
            new Usuario().NovoUsuario(user, pass, email);
            return RedirectToAction("Login");
        }
    }
}