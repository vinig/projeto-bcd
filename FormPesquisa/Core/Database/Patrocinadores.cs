﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database
{
    [Table("BCD_Patrocinadores")]
    public class Patrocinadores : DefaultDatabase<Patrocinadores>
    {
        [Key]
        public int id { get; set; }
        public string nome { get; set; }
        public string imagem { get; set; }

        public void Insert(string nome, string imagem)
        {
            ExecuteQuery<int>(@"INSERT INTO BCD_Patrocinadores VALUES(@nome,@imagem)", new { nome, imagem });
        }
        public int Count()
        {
            return ExecuteQuery<int>(@"SELECT COUNT(id) FROM BCD_Patrocinadores").FirstOrDefault();
        }

    }
}
