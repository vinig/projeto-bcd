﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database
{
    [Table("BCD_Times")]
    public class Times : DefaultDatabase<Times>
    {
        [Key]
        public int id { get; set; }
        public string nome { get; set; }
        public int paisId { get; set; }
        public int ligaId { get; set; }

        public List<dynamic> TimesAll()
        {
            return ExecuteQuery<dynamic>(@" SELECT DISTINCT T.id, T.nome Time, L.nome Liga, P.nome Pais, COUNT(J.id) Jogadores FROM BCD_Times T
                                            INNER JOIN BCD_Ligas L ON L.id = T.ligaId
                                            INNER JOIN BCD_Paises P ON P.id = T.paisId
                                            LEFT JOIN BCD_Jogadores J ON J.timeId = T.id
                                            GROUP BY T.id, T.nome, L.nome, P.nome");
        }

        public void TimesIU(Times obj)
        {
            if (obj.id != 0)
                ExecuteQuery<Times>(@"UPDATE BCD_Times SET nome = @nome, paisId = @paisId, ligaId = @ligaId WHERE id = @id", 
                    new { nome = obj.nome, paisId = obj.paisId, ligaId = obj.ligaId, id = obj.id });
            else
                ExecuteQuery<Times>(@"INSERT INTO BCD_Times VALUES (@nome, @paisId, @ligaId)", new { nome = obj.nome, paisId = obj.paisId, ligaId = obj.ligaId });
        }

        public void TimeDeletar(Times obj)
        {
            ExecuteQuery<Times>(@"DELETE FROM BCD_Times WHERE id = @id", new { obj.id });
        }

        public List<dynamic> TimesJOIN()
        {
            return ExecuteQuery<dynamic>(@" SELECT TOP(5) T.nome Time, P.nome Pais, L.nome Liga FROM BCD_Times T
                                            FULL JOIN BCD_Ligas L ON L.id = T.ligaId
                                            LEFT JOIN BCD_Paises P ON P.id = T.paisId
                                            ORDER BY P.id DESC");
        }
    }
}
