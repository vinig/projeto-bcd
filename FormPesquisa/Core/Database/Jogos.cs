﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database
{
    [Table("BCD_Jogos")]
    public class Jogos : DefaultDatabase<Jogos>
    {
        [Key]
        public int id { get; set; }
        public string nome { get; set; }
        public int casaTimeId { get; set; }
        public int foraTimeId { get; set; }
        public int ligaId { get; set; }
        public int paisId { get; set; }

        public List<dynamic> JogosAll()
        {
            return ExecuteQuery<dynamic>(@" SELECT J.id, J.nome, J.casaTimeId mandante, J.foraTimeId visitante, L.nome liga, P.nome pais FROM BCD_Jogos J
                                            INNER JOIN BCD_Times T ON T.id = J.id
                                            INNER JOIN BCD_Ligas L ON L.id = J.ligaId
                                            INNER JOIN BCD_Paises P ON P.id = J.paisId");
        }

        public void JogoIU(Jogos obj)
        {
            if (obj.id != 0)
                ExecuteQuery<Jogos>(@"  UPDATE BCD_Jogos SET nome = @nome, casaTimeId = @casaTimeId, 
                                        foraTimeId = @foraTimeId, ligaId = @ligaId, paisId = @paisId WHERE id = @id",
                                        new { nome = obj.nome, casaTimeId = obj.casaTimeId,
                                        foraTimeId = obj.foraTimeId, ligaId = obj.ligaId, paisId = obj.paisId, id = obj.id });
            else
                ExecuteQuery<Jogos>(@"  INSERT INTO BCD_Jogos VALUES (@nome,@casaTimeId,@foraTimeId,@ligaId,@paisId)", 
                                        new { nome = obj.nome, casaTimeId = obj.casaTimeId,
                                        foraTimeId = obj.foraTimeId, ligaId = obj.ligaId, paisId = obj.paisId});
        }

        public void JogoDeletar(Jogos obj)
        {
            ExecuteQuery<Jogos>(@"DELETE FROM BCD_Jogos WHERE id = @id", new { obj.id });
        }
    }
}

