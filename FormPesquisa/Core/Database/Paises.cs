﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database
{
    [Table("BCD_Paises")]
    public class Paises : DefaultDatabase<Paises>
    {
        [Key]
        public int id { get; set; }
        public string nome { get; set; }

        public void Insert(string nome)
        {
            ExecuteQuery<int>(@"INSERT INTO BCD_Paises VALUES(@nome)", new { nome });
        }
        public int Count()
        {
            return ExecuteQuery<int>(@"SELECT COUNT(id) FROM BCD_Paises").FirstOrDefault();
        }

        public List<dynamic> PaisesAND()
        {
            return ExecuteQuery<dynamic>(@" SELECT P.nome, T.nome Time FROM BCD_Paises P
                                            INNER JOIN BCD_Times T ON T.paisId = P.id
                                            INNER JOIN BCD_Ligas L ON L.id = T.ligaId
                                            WHERE P.id = (SELECT TOP(1) id FROM BCD_Paises) AND T.id = (SELECT TOP(1) id FROM BCD_Times)");
        }
    }
}
