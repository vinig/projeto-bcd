﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Core.Database.System;
using Dapper;
using Core.Database;

namespace Core
{
    public abstract class Connection<TEntity> : IRepositories<TEntity> where TEntity : class
    {
        protected string ConnectionString;

        public dynamic Multiple(int position, string proc, object param = null)
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                try
                {
                    using (var multi = conn.QueryMultiple(proc, param,
                        commandType: CommandType.StoredProcedure))
                    {

                        List<dynamic> magicReturn = new List<dynamic>();
                        for (int i = 0; i < position; i++)
                        {
                            magicReturn.Add(multi.Read().ToList());
                        }
                        return magicReturn;
                    }

                }
                catch (SqlException ex)
                {
                    new Logs() { Date = DateTime.Now, Message = ex.Message }.Insert();
                    throw new ApplicationException(ex.Message);
                }
                catch (Exception ex)
                {
                    new Logs() { Date = DateTime.Now, Message = ex.Message }.Insert();
                    throw new Exception("Erro ao obter todos os elementos da entidade no banco.", ex);
                }
            }
        }

        public List<TEntity> All()
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                try
                {
                    return conn.GetList<TEntity>().AsList();
                }
                catch (SqlException ex)
                {
                    new Logs() { Date = DateTime.Now, Message = ex.Message }.Insert();
                    throw new ApplicationException(ex.Message);
                }
                catch (Exception ex)
                {
                    new Logs() { Date = DateTime.Now, Message = ex.Message }.Insert();
                    throw new Exception("Erro ao obter todos os elementos da entidade no banco.", ex);
                }
            }
        }

        public TEntity Get(int id)
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                try
                {
                    return conn.Get<TEntity>(id);
                }
                catch (SqlException ex)
                {
                    new Logs() { Date = DateTime.Now, Message = ex.Message }.Insert();
                    throw new ApplicationException(ex.Message);
                }
                catch (Exception ex)
                {
                    new Logs() { Date = DateTime.Now, Message = ex.Message }.Insert();
                    throw new Exception("Erro ao obter entidade no banco.", ex);
                }
            }
        }

        public List<T> GetByParams<T>(object whereConditions)
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                try
                {
                    return conn.GetList<T>(whereConditions).AsList();
                }
                catch (SqlException ex)
                {
                    new Logs() { Date = DateTime.Now, Message = ex.Message }.Insert();
                    throw new ApplicationException(ex.Message);
                }
                catch (Exception ex)
                {
                    new Logs() { Date = DateTime.Now, Message = ex.Message }.Insert();
                    throw new Exception("Erro ao obter entidade no banco.", ex);
                }
            }
        }

        public List<T> GetByParams<T>(string query, object whereConditions)
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                try
                {
                    return conn.GetList<T>(query, whereConditions).AsList();
                }
                catch (SqlException ex)
                {
                    new Logs() { Date = DateTime.Now, Message = ex.Message }.Insert();
                    throw new ApplicationException(ex.Message);
                }
                catch (Exception ex)
                {
                    new Logs() { Date = DateTime.Now, Message = ex.Message }.Insert();
                    throw new Exception("Erro ao obter entidade no banco.", ex);
                }
            }
        }

        public int? Insert(int? timeout = null)
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                try
                {
                    return conn.Insert(this, commandTimeout: timeout);
                }
                catch (SqlException ex)
                {
                    new Logs() { Date = DateTime.Now, Message = ex.Message }.Insert();
                    throw new ApplicationException(ex.Message);
                }
                catch (Exception ex)
                {
                    new Logs() { Date = DateTime.Now, Message = ex.Message }.Insert();
                    throw new Exception("Erro ao inserir entidade no banco.", ex);
                }
            }
        }


        public int Update()
        {
            try
            {
                using (var conn = new SqlConnection(ConnectionString))
                    return conn.Update(this);
            }
            catch (SqlException ex)
            {
                new Logs() { Date = DateTime.Now, Message = ex.Message }.Insert();
                throw new ApplicationException(ex.Message);
            }
            catch (Exception ex)
            {
                new Logs() { Date = DateTime.Now, Message = ex.Message }.Insert();
                throw new Exception("Erro ao atualizar entidade no banco.", ex);
            }
        }

        public virtual int Delete()
        {
            try
            {
                using (var conn = new SqlConnection(ConnectionString))
                    return conn.Delete(this);
            }
            catch (SqlException ex)
            {
                new Logs() { Date = DateTime.Now, Message = ex.Message }.Insert();
                throw new ApplicationException(ex.Message);
            }
            catch (Exception ex)
            {
                new Logs() { Date = DateTime.Now, Message = ex.Message }.Insert();
                throw new Exception("Erro ao deletar entidade no banco.", ex);
            }
        }

        public List<T> ExecuteQuery<T>(string query, object param = null, int? timeout = 60)
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                try
                {
                    return conn.Query<T>(query, param, commandTimeout: timeout).AsList();
                }
                catch (SqlException ex)
                {
                    if (query.StartsWith("insert into logs", StringComparison.InvariantCultureIgnoreCase))
                    {
                        Console.WriteLine("ERRO AO GRAVAR LOGS: Query: " + query + " param: " + param);
                        var message = "Erro ao executar comando.";
                        if (param != null)
                        {
                            message = ((dynamic)param).message;
                        }
                        throw new ApplicationException(message);
                    }

                    new Logs() { Message = ex.Message, Query = param.ToString(), Date = DateTime.Now }.Insert();
                    throw new ApplicationException(ex.Message);
                }
                catch (Exception ex)
                {
                    if (query.StartsWith("insert into logs", StringComparison.InvariantCultureIgnoreCase))
                    {
                        Console.WriteLine("ERRO AO GRAVAR LOGS: Query: " + query + " param: " + param);
                    }
                    else
                    {
                        new Logs() { Message = ex.Message, Query = query.ToString(), Date = DateTime.Now }.Insert();
                    }
                    throw new Exception("Erro ao executar query no banco.", ex);
                }
            }
        }

        public int ExecuteStoredProcedure(string storedProcedure, DynamicParameters parameters = null, int? timeout = null)
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                try
                {
                    return conn.Execute(storedProcedure, parameters, commandType: CommandType.StoredProcedure, commandTimeout: timeout);
                }
                catch (SqlException ex)
                {
                    new Logs() { Message = ex.Message, Query = storedProcedure + " " + parameters, Date = DateTime.Now }.Insert();
                    throw new ApplicationException(ex.Message);
                }
                catch (Exception ex)
                {
                    new Logs() { Message = ex.Message, Query = storedProcedure + " " + parameters, Date = DateTime.Now }.Insert();
                    throw new Exception("Erro ao executar stored procedure no banco.", ex);
                }
            }
        }

        public object GetPropertyValue(string propertyName)
            => GetType().GetProperties().Single(pi => pi.Name == propertyName).GetValue(this, null);


        public void SetPropertyValue(string propertyName, object value)
            => GetType().GetProperties().Single(pi => pi.Name == propertyName).SetValue(this, value, null);

        public List<T> GetByParamsPaged<T>(int page, int results, string whereConditions)
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                try
                {
                    return conn.GetListPaged<T>(page, results, whereConditions, null).AsList();
                }
                catch (SqlException ex)
                {
                    new Logs() { Date = DateTime.Now, Message = ex.Message }.Insert();
                    throw new ApplicationException(ex.Message);
                }
                catch (Exception ex)
                {
                    new Logs() { Date = DateTime.Now, Message = ex.Message }.Insert();
                    throw new Exception("Erro ao obter entidade no banco.", ex);
                }
            }
        }

        public List<TEntity> GetByCondition(string conditions, object parameters = null)
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                try
                {
                    return conn.GetList<TEntity>(conditions, parameters).AsList();
                }
                catch (SqlException ex)
                {
                    new Logs() { Date = DateTime.Now, Message = ex.Message }.Insert();
                    throw new ApplicationException(ex.Message);
                }
                catch (Exception ex)
                {
                    new Logs() { Date = DateTime.Now, Message = ex.Message }.Insert();
                    throw new Exception("Erro ao obter entidade no banco.", ex);
                }
            }
        }

        public List<IDictionary<string, object>> ExecuteQuery(string query, object param = null)
        {
            return ExecuteQuery<object>(query, param).Cast<IDictionary<string, object>>().ToList();
        }

    }
}
