﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database
{
    [Table("BCD_Campeoes")]
    public class Campeoes : DefaultDatabase<Campeoes>
    {
        [Key]
        public int id { get; set; }
        public int ano { get; set; }
        public int timeId { get; set; }

        public List<dynamic> CampeoesAll()
        {
            return ExecuteQuery<dynamic>(@" SELECT C.*, T.nome Time FROM BCD_Campeoes C
                                            INNER JOIN BCD_Times T ON T.id = C.timeId");
        }

        public void CampeoesIU(Campeoes obj)
        {
            if (obj.id != 0)
                ExecuteQuery<Campeoes>(@"UPDATE BCD_Campeoes SET ano = @ano, timeId = @timeId WHERE id = @id",
                    new { ano = obj.ano, timeId = obj.timeId, id = obj.id });
            else
                ExecuteQuery<Campeoes>(@"INSERT INTO BCD_Campeoes VALUES (@ano, @timeId)", new { ano = obj.ano, timeId = obj.timeId });
        }

        public void CampeaoDeletar(Campeoes obj)
        {
            ExecuteQuery<Campeoes>(@"DELETE FROM BCD_Campeoes WHERE id = @id", new { obj.id });
        }
    }
}
