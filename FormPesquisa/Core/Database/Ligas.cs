﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database
{
    [Table("BCD_Ligas")]
    public class Ligas : DefaultDatabase<Ligas>
    {
        [Key]
        public int id { get; set; }
        public string nome { get; set; }

        public void Insert(string nome)
        {
            ExecuteQuery<int>(@"INSERT INTO BCD_Ligas VALUES(@nome)", new { nome });
        }
        public int Count()
        {
            return ExecuteQuery<int>(@"SELECT COUNT(id) FROM BCD_Ligas").FirstOrDefault();
        }

        public List<dynamic> LigasOR()
        {
            return ExecuteQuery<dynamic>(@" SELECT TOP(5) L.id LigaId, L.nome Liga, T.id, T.nome FROM BCD_Ligas L
                                            INNER JOIN BCD_Times T ON T.ligaId = L.id
                                            WHERE L.id > 1 OR T.id< 3");
        }
    }
}

