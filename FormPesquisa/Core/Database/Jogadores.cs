﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database
{
    [Table("BCD_Jogadores")]
    public class Jogadores : DefaultDatabase<Jogadores>
    {
        [Key]
        public int id { get; set; }
        public string nome { get; set; }
        public int numero  { get; set; }
        public int timeId  { get; set; }
        public int paisId { get; set; }
        public int ligaId { get; set; }
        public int posicaoId { get; set; }
        public bool titular { get; set; }

        public List<dynamic> JogadoresAll()
        {
            return ExecuteQuery<dynamic>(@" SELECT J.id, J.nome, J.numero, T.nome Time, L.nome Liga, PA.nome Pais, P.sigla, P.nome Posicao, J.titular  FROM BCD_Jogadores J
                                            INNER JOIN BCD_Times T ON T.id = J.timeId
                                            INNER JOIN BCD_Ligas L ON L.id = J.ligaId
                                            INNER JOIN BCD_Paises PA ON PA.id = J.paisId
                                            INNER JOIN BCD_Posicoes P ON P.id = J.posicaoId");
        }

        public void JogadorIU(Jogadores obj)
        {
            if (obj.id != 0)
                ExecuteQuery<Jogadores>(@"  UPDATE BCD_Jogadores SET nome = @nome, numero = @numero, 
                                            timeId = @timeId, paisId = @paisId, ligaId = @ligaId, posicaoId = @posicaoId, titular = @titular WHERE id = @id",
                                            new
                                            {
                                                nome = obj.nome,
                                                numero = obj.numero,
                                                timeId = obj.timeId,
                                                paisId = obj.paisId,
                                                ligaId = obj.ligaId,
                                                posicaoId = obj.posicaoId,
                                                titular = obj.titular,
                                                id = obj.id
                                            });
            else
                ExecuteQuery<Jogadores>(@"  INSERT INTO BCD_Jogadores VALUES (@nome,@numero,@timeId,@paisId,@ligaId,@posicaoId,@titular)",
                                        new
                                        {
                                            nome = obj.nome,
                                            numero = obj.numero,
                                            timeId = obj.timeId,
                                            paisId = obj.paisId,
                                            ligaId = obj.ligaId,
                                            posicaoId = obj.posicaoId,
                                            titular = obj.titular
                                        });
        }

        public void JogadorDeletar(Jogadores obj)
        {
            ExecuteQuery<Jogadores>(@"DELETE FROM BCD_Jogadores WHERE id = @id", new { obj.id });
        }


        public List<dynamic> JogadoresBetWeen(int inicio, int fim)
        {
            return ExecuteQuery<dynamic>(@" SELECT TOP(5) J.numero, J.nome, T.nome Time FROM BCD_Jogadores J
                                            INNER JOIN BCD_Times T ON T.id = J.timeId
                                            WHERE numero BETWEEN @inicio AND @fim", new { inicio, fim });
        }

        public List<dynamic> JogadoresSUM()
        {
            return ExecuteQuery<dynamic>(@" SELECT T.nome, SUM(CONVERT(INT, J.titular)) Titulares from BCD_Jogadores J
                                            INNER JOIN BCD_Times T ON T.id = J.timeId
                                            GROUP BY T.nome
                                            ORDER BY T.nome DESC");
        }

        public List<dynamic> JogadoresNOT()
        {
            return ExecuteQuery<dynamic>(@" SELECT TOP(5) J.nome, P.nome Posicao, J.titular FROM BCD_Jogadores J
                                            RIGHT JOIN  BCD_Posicoes P ON P.id = J.posicaoId
                                            WHERE NOT titular = 1");
        }       
    }
}
