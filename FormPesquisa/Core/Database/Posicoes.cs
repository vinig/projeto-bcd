﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Database
{
    [Table("BCD_Posicoes")]
    public class Posicoes : DefaultDatabase<Posicoes>
    {
        [Key]
        public int id { get; set; }
        public string nome { get; set; }
        public string sigla { get; set; }

        public List<Posicoes> PosicaoAll()
        {
            return ExecuteQuery<Posicoes>(@"SELECT * FROM BCD_Posicoes ORDER BY id DESC");
        }

        public int PosicoesAVG()
        {
            return ExecuteQuery<int>(@"SELECT AVG(id) media FROM BCD_Posicoes").FirstOrDefault();
        }
    }
}

