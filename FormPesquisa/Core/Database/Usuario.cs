﻿using System;
using Dapper;
using System.Linq;

namespace Core.Database.System
{
    [Table("BCD_Usuario")]
    public class Usuario : DefaultDatabase<Usuario>
    {
        [Key]
        public int id { get; set; }
        public string nome { get; set; }
        public string senha { get; set; }
        public string email { get; set; }

        public int UsuarioLogin(string nome, string senha)
        {
            return ExecuteQuery<int>(@"SELECT * FROM BCD_Usuario WHERE nome = @nome and senha = @senha", new { nome, senha }).FirstOrDefault();
        }

        public void NovoUsuario(string nome, string senha, string email)
        {
            ExecuteQuery<int>(@"INSERT INTO BCD_Usuario VALUES (@nome,@senha,@email)", new { nome, senha, email });
        }

    }
}