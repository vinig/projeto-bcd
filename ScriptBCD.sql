CREATE TABLE BCD_Usuario (
	id int primary key identity(1,1),
	nome varchar(100),
	senha varchar(50),
	email varchar(100)
);

INSERT INTO BCD_Usuario VALUES ('admin', '123', 'admin@admin.com')

CREATE TABLE BCD_Patrocinadores (
	id int primary key identity(1,1),
	nome varchar(100),
	imagem varchar(max)
);

CREATE TABLE BCD_Paises (
	id int primary key identity(1,1),
	nome varchar(100)
);

INSERT INTO BCD_Paises VALUES ('Inglaterra');

CREATE TABLE BCD_Ligas (
	id int primary key identity(1,1),
	nome varchar(100)
);

INSERT INTO BCD_Ligas VALUES ('Premier League');


CREATE TABLE BCD_Times (
	id int primary key identity(1,1),
	nome varchar(100),
	paisId int FOREIGN KEY REFERENCES BCD_Paises(id),
	ligaId int FOREIGN KEY REFERENCES BCD_Ligas(id),
);

INSERT INTO BCD_Times VALUES ('Liverpool Football Club', 1,1);
INSERT INTO BCD_Times VALUES ('Tottenham Hotspur Football Club', 1,1);


CREATE TABLE BCD_Jogos (
	id int primary key identity(1,1),
	nome varchar(100),
	casaTimeId int FOREIGN KEY REFERENCES BCD_Times(id),
	foraTimeId int FOREIGN KEY REFERENCES BCD_Times(id),
	ligaId int FOREIGN KEY REFERENCES BCD_Ligas(id),
	paisId int FOREIGN KEY REFERENCES BCD_Paises(id)
);

INSERT INTO BCD_Jogos VALUES ('Final Champions',1,2,1,1)

CREATE TABLE BCD_Posicoes (
	id int primary key identity(1,1),
	nome varchar(100),
	sigla varchar(10)
);

INSERT INTO BCD_Posicoes VALUES ('Goleiro', 'GOL')
INSERT INTO BCD_Posicoes VALUES ('Zagueiro', 'ZAG')
INSERT INTO BCD_Posicoes VALUES ('Lateral', 'LAT')
INSERT INTO BCD_Posicoes VALUES ('Volante', 'VOL')
INSERT INTO BCD_Posicoes VALUES ('Meio Campo', 'MC')
INSERT INTO BCD_Posicoes VALUES ('Atacante', 'ATA')

CREATE TABLE BCD_Jogadores (
	id int primary key identity(1,1),
	nome varchar(100),
	numero int,
	timeId int FOREIGN KEY REFERENCES BCD_Times(id),
	paisId int FOREIGN KEY REFERENCES BCD_Paises(id),
	ligaId int FOREIGN KEY REFERENCES BCD_Ligas(id),
	posicaoId int FOREIGN KEY REFERENCES BCD_Posicoes(id),
	titular bit default 0
);
INSERT INTO BCD_Jogadores VALUES ('Jogador',0,1,1,1,1,1)

CREATE TABLE BCD_Campeoes (
	id int primary key identity(1,1),
	ano int,
	timeId int FOREIGN KEY REFERENCES BCD_Times(id)
);

INSERT INTO BCD_Campeoes VALUES (2019,1);
